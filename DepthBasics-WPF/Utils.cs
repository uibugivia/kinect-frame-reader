﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Samples.Kinect.DepthBasics
{
    class Utils
    {
        public static string generate_filename(int n, int len)
        {
            string str_n = n.ToString();
            if (str_n.Length > len)
            {
                return str_n;
            }
            string output = "";
            for (int i=0;i<len-str_n.Length; i++)
            {
                output += "0";
            }

            return output + str_n;
        }

        public static void check_dir(string path)
        {
            bool exists = System.IO.Directory.Exists(path);

            if (!exists)
                System.IO.Directory.CreateDirectory(path);
        }
    }
}
