﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Samples.Kinect.DepthBasics
{
	class Config
	{
		/// <summary>
		/// Seconds between frames
		/// </summary>
		public const double frames_interval = 0.05;
		public const string PATH_COLOR_FRAMES = @"D:\frames1\color\";
		public const string PATH_DEPTH_FRAMES = @"D:\frames1\depth\";
		public const string PATH_NOBG_FRAMES = @"D:\frames1\noBG\";
		public const int LEN_FILENAMES = 7;

	}
}
