﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.DepthBasics
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using Microsoft.Kinect;
    using KinectBackgroundRemoval;
	
    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private int frame_number = 0;
        DispatcherTimer timer;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;
        
        /// <summary>
        /// Reader for depth/color/body index frames
        /// </summary>
        private MultiSourceFrameReader multiFrameSourceReader = null;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        FrameProcessing fProcessing;

		FrameWritter writter;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            // get the kinectSensor object
            this.kinectSensor = KinectSensor.GetDefault();

            this.multiFrameSourceReader = this.kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Depth | FrameSourceTypes.Color | FrameSourceTypes.BodyIndex);

            this.multiFrameSourceReader.MultiSourceFrameArrived += this.Reader_MultiSourceFrameArrived;
            
            // set IsAvailableChanged event notifier
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            // open the sensor
            this.kinectSensor.Open();

            // 2) Initialize the background removal tool.
            BackgroundRemovalTool _backgroundRemovalTool = new BackgroundRemovalTool(kinectSensor.CoordinateMapper);

            // get FrameDescription from DepthFrameSource
            this.fProcessing = new FrameProcessing(
                this.kinectSensor.DepthFrameSource.FrameDescription,
                this.kinectSensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Bgra),
                this.kinectSensor.CoordinateMapper,
                _backgroundRemovalTool);

			this.writter = new FrameWritter(this.fProcessing);

            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            // use the window object as the view model in this simple example
            this.DataContext = this;

            // initialize the components (controls) of the window
            this.InitializeComponent();

            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(time_event);
            timer.Interval = TimeSpan.FromSeconds(Config.frames_interval);
            timer.Start();
        }

        void onLoaded(object sender, EventArgs e)
        {
           
        }
        void time_event(object sender, EventArgs e)
        {
            capture_frame();
        }

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource colorSource
        {
            get
            {
                return this.fProcessing.colorSource;
            }
        }

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource depthSource
        {
            get
            {
                return this.fProcessing.depthSource;
            }
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.multiFrameSourceReader != null)
            {
                // MultiSourceFrameReder is IDisposable
                this.multiFrameSourceReader.Dispose();
                this.multiFrameSourceReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        void capture_frame()
        {
            if (this.fProcessing.depthBitmap != null && this.fProcessing.ProcessedColorFrame != null)
            {
                this.frame_number++;
                if (writter.save_depth_frame(frame_number))
				{
					this.StatusText = string.Format(CultureInfo.CurrentCulture, 
						Properties.Resources.SavedScreenshotStatusTextFormat, writter.path);
				}
				else
				{
					this.StatusText = string.Format(CultureInfo.CurrentCulture, 
						Properties.Resources.FailedScreenshotStatusTextFormat, writter.path);
				}
				if (writter.save_color_frame(frame_number))
				{
					this.StatusText = string.Format(Properties.Resources.SavedScreenshotStatusTextFormat, writter.path);
				}
				else
				{
					this.StatusText = string.Format(Properties.Resources.FailedScreenshotStatusTextFormat, writter.path);
				}
				if (writter.save_noBG_frame(frame_number))
				{
					this.StatusText = string.Format(Properties.Resources.SavedScreenshotStatusTextFormat, writter.path);
				}
				else
				{
					this.StatusText = string.Format(Properties.Resources.FailedScreenshotStatusTextFormat, writter.path);
				}
            }
        }


        /// <summary>
        /// Handles the depth/color/body index frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            MultiSourceFrame multiFrame = e.FrameReference.AcquireFrame();
            fProcessing.process_frames(multiFrame);
        }

        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            // on failure, set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;
        }
    }
}
