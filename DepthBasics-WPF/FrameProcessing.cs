﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Threading;

using Microsoft.Kinect;
using KinectBackgroundRemoval;

namespace Microsoft.Samples.Kinect.DepthBasics
{
    class FrameProcessing
    {

        FrameDescription colorFrameDescription;

        /// <summary>
        /// Description of the data contained in the depth frame
        /// </summary>
        private FrameDescription depthFrameDescription = null;

        /// <summary>
        /// Bitmap to display
        /// </summary>
        public WriteableBitmap depthBitmap = null;

        public WriteableBitmap rawPNG = null;
        /// <summary>
        /// Bitmap to display
        /// </summary>
        private WriteableBitmap colorBitmap = null;

        /// <summary>
        /// Bitmap to save in file
        /// </summary>
        private BitmapSource processedBitmap = null;

        //private ushort[] depthData = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Intermediate storage for frame data converted to color
        /// </summary>
        private byte[] depthPixels = null;

        private ushort[] rawDepth = null;

        //private byte[] colorData = null;
        //private byte[] colorMapped = null;
        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private readonly int bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;

        /// <summary>
        /// Map depth range to byte range
        /// </summary>
        private const int MapDepthToByte = 8000 / 256;

		/// <summary>
		/// Wrap of background removal
		/// </summary>
		BackgroundRemovalTool _backgroundRemovalTool;


        public FrameProcessing(FrameDescription depthFrameDescription,
            FrameDescription colorFrameDescription,
            CoordinateMapper coordinateMapper,
            BackgroundRemovalTool backgroundRemovalTool)
        {
            this.depthFrameDescription = depthFrameDescription;
            this.coordinateMapper = coordinateMapper;
            this.colorFrameDescription = colorFrameDescription;
            this._backgroundRemovalTool = backgroundRemovalTool;
            int colorWidth = colorFrameDescription.Width;
            int colorHeight = colorFrameDescription.Height;

            // create the bitmap to display
            this.colorBitmap = new WriteableBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Bgr32, null);

            // create the bitmap to display
            this.processedBitmap = new WriteableBitmap(this.depthFrameDescription.Width, this.depthFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null);

            // create the bitmap to display
            this.depthBitmap = new WriteableBitmap(this.depthFrameDescription.Width, this.depthFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray8, null);

            // create the bitmap to display
            this.rawPNG = new WriteableBitmap(this.depthFrameDescription.Width, this.depthFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray16, null);

            // allocate space to put the pixels being received and converted
            this.depthPixels = new byte[this.depthFrameDescription.Width * this.depthFrameDescription.Height];
            this.rawDepth = new ushort[this.depthFrameDescription.Width * this.depthFrameDescription.Height];
        }

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource colorSource
        {
            get
            {
                return this.colorBitmap;
            }
        }

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource depthSource
        {
            get
            {
                return this.depthBitmap;
            }
        }

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource depthPNG
        {
            get
            {
                return this.rawPNG;
            }
        }
		
		public ImageSource ProcessedColorFrame
		{
			get
			{
				return processedBitmap;
			}
		}

		/// <summary>
		/// Handles the depth/color/body index frame data arriving from the sensor
		/// </summary>
		/// <param name="sender">object sending the event</param>
		/// <param name="e">event arguments</param>
		public void process_frames(MultiSourceFrame multiFrame)
        {
            bool depthFrameProcessed = false;
            using (var colorFrame = multiFrame.ColorFrameReference.AcquireFrame())
            using (var depthFrame = multiFrame.DepthFrameReference.AcquireFrame())
            using (var bodyIndexFrame = multiFrame.BodyIndexFrameReference.AcquireFrame())
            {
                if (depthFrame != null && colorFrame != null && bodyIndexFrame != null)
                {
                    // the fastest way to process the body index data is to directly access 
                    // the underlying buffer
                    using (KinectBuffer depthBuffer = depthFrame.LockImageBuffer())
                    {
                        // verify data and write the color data to the display bitmap
                        if (((this.depthFrameDescription.Width * this.depthFrameDescription.Height) == (depthBuffer.Size / this.depthFrameDescription.BytesPerPixel)) &&
                            (this.depthFrameDescription.Width == this.depthBitmap.PixelWidth) && (this.depthFrameDescription.Height == this.depthBitmap.PixelHeight))
                        {
                            // Note: In order to see the full range of depth (including the less reliable far field depth)
                            // we are setting maxDepth to the extreme potential depth threshold
                            //ushort maxDepth = ushort.MaxValue;

                            // If you wish to filter by reliable depth distance, uncomment the following line:
                            //// maxDepth = depthFrame.DepthMaxReliableDistance

                            //this.ProcessDepthFrameData(depthBuffer.UnderlyingBuffer, depthBuffer.Size, depthFrame.DepthMinReliableDistance, maxDepth);
                            depthFrameProcessed =true;

                        }
                    }
                    
                    FrameDescription colorFrameDescription = colorFrame.FrameDescription;

                    using (KinectBuffer colorBuffer = colorFrame.LockRawImageBuffer())
                    {

                        //this.processedBitmap = this._backgroundRemovalTool.GreenScreen(colorFrame, depthFrame, bodyIndexFrame);
                        this.colorBitmap.Lock();
                        byte[] colorData = new byte[colorFrameDescription.Width * colorFrameDescription.Height * this.bytesPerPixel];
                        //this.colorMapped = new byte[depthFrameDescription.Width * depthFrameDescription.Height * this.bytesPerPixel];
                        colorFrame.CopyConvertedFrameDataToArray(colorData, ColorImageFormat.Bgra);

                        this.colorBitmap.WritePixels(
							new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight), 
							colorData, this.colorBitmap.BackBufferStride, 0);
                        this.colorBitmap.Unlock();
                    }
                }
            }

            if (depthFrameProcessed)
            {
                this.RenderDepthPixels();
            }
        }


        /// <summary>
        /// Directly accesses the underlying image buffer of the DepthFrame to 
        /// create a displayable bitmap.
        /// This function requires the /unsafe compiler option as we make use of direct
        /// access to the native memory pointed to by the depthFrameData pointer.
        /// </summary>
        /// <param name="depthFrameData">Pointer to the DepthFrame image data</param>
        /// <param name="depthFrameDataSize">Size of the DepthFrame image data</param>
        /// <param name="minDepth">The minimum reliable depth value for the frame</param>
        /// <param name="maxDepth">The maximum reliable depth value for the frame</param>
        private unsafe void ProcessDepthFrameData(IntPtr depthFrameData, uint depthFrameDataSize, ushort minDepth, ushort maxDepth)
        {
            // depth frame data is a 16 bit value
            ushort* frameData = (ushort*)depthFrameData;

            // convert depth to a visual representation
            for (int i = 0; i < (int)(depthFrameDataSize / this.depthFrameDescription.BytesPerPixel); ++i)
            {
                // Get the depth for this pixel
                ushort depth = frameData[i];

                // To convert to a byte, we're mapping the depth value to the byte range.
                // Values outside the reliable depth range are mapped to 0 (black).
                //this.depthPixels[i] = (byte)(depth >= minDepth && depth <= maxDepth ? (depth / MapDepthToByte) : 0);
                this.rawDepth[i] = (ushort) (depth >= minDepth && depth <=maxDepth ? (depth) : 0);
            }
        }

        /// <summary>
        /// Renders color pixels into the writeableBitmap.
        /// </summary>
        private void RenderDepthPixels()
        {
            //this.depthBitmap.WritePixels(
            //    new Int32Rect(0, 0, this.depthBitmap.PixelWidth, this.depthBitmap.PixelHeight),
            //    this.depthPixels,
            //    this.depthBitmap.PixelWidth,
            //    0);

            this.rawPNG.WritePixels(
                new Int32Rect(0, 0, this.depthBitmap.PixelWidth, this.depthBitmap.PixelHeight),
                this.rawDepth,
                this.depthBitmap.PixelWidth*2,
                0);
        }
    }
}
