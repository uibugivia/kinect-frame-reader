﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Microsoft.Samples.Kinect.DepthBasics
{
	class FrameWritter
	{
		/// <summary>
		/// Where the last file was saved
		/// </summary>
		public string path;

		private FrameProcessing frame;

		public FrameWritter(FrameProcessing frame)
		{
			this.frame = frame;

			Utils.check_dir(Config.PATH_COLOR_FRAMES);
			Utils.check_dir(Config.PATH_DEPTH_FRAMES);
			Utils.check_dir(Config.PATH_NOBG_FRAMES);
		}

		public bool save_depth_frame(int nFrame)
		{
			BitmapEncoder encoder = new PngBitmapEncoder();

			// create a png bitmap encoder which knows how to save a .png file
			var gray16Bitmap = new FormatConvertedBitmap(this.frame.rawPNG,
				System.Windows.Media.PixelFormats.Gray16, null, 0d);

			// create frame from the writable bitmap and add to encoder
			encoder.Frames.Add(BitmapFrame.Create(gray16Bitmap));

			path = Path.Combine(Config.PATH_DEPTH_FRAMES, Utils.generate_filename(nFrame, Config.LEN_FILENAMES) + ".png");

			// write the new file to disk
			try
			{
				// FileStream is IDisposable
				using (FileStream fs = new FileStream(path, FileMode.Create))
				{
					encoder.Save(fs);
				}

				return true;
			}
			catch (IOException)
			{
				return false;
			}
		}

		public bool save_color_frame(int nFrame)
		{
			// create a png bitmap encoder which knows how to save a .png file
			BitmapEncoder encoder = new PngBitmapEncoder();

			// create frame from the writable bitmap and add to encoder
			encoder.Frames.Add(BitmapFrame.Create((WriteableBitmap)this.frame.colorSource));

			path = Path.Combine(Config.PATH_COLOR_FRAMES, Utils.generate_filename(nFrame, Config.LEN_FILENAMES) + ".png");

			// write the new file to disk
			try
			{
				// FileStream is IDisposable
				using (FileStream fs = new FileStream(path, FileMode.Create))
				{
					encoder.Save(fs);
				}

				return true;
			}
			catch (IOException)
			{
				return false;
			}
		}

		public bool save_noBG_frame(int nFrame)
		{
			// create a png bitmap encoder which knows how to save a .png file
			BitmapEncoder encoder = new PngBitmapEncoder();

			// create frame from the writable bitmap and add to encoder
			encoder.Frames.Add(BitmapFrame.Create((WriteableBitmap) this.frame.ProcessedColorFrame));

			path = Path.Combine(Config.PATH_NOBG_FRAMES, Utils.generate_filename(nFrame, Config.LEN_FILENAMES) + ".png");

			// write the new file to disk
			try
			{
				// FileStream is IDisposable
				using (FileStream fs = new FileStream(path, FileMode.Create))
				{
					encoder.Save(fs);
				}
				return true;
			}
			catch (IOException)
			{
				return false;
			}
		}

	}
}
